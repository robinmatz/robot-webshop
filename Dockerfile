FROM python:3.9-slim

ENV WORK_DIR=/opt/robotframework

ENV VIRTUAL_ENV $WORK_DIR/.venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH "$VIRTUAL_ENV:$PATH"

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r \
    requirements.txt
