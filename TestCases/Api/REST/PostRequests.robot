*** Settings ***
Resource        ../../../Resources/Settings.resource

Suite Setup     Create Session    reqres    ${ENDPOINT}    verify=True

*** Variables ***
${ENDPOINT}     https://reqres.in

*** Test Cases ***
Create User
    ${request}    Create Dictionary    user=robert    job=cook
    ${resp}    POST On Session    reqres    /api/users    json=${request}    expected_status=201
    Dictionary Should Contain Key    ${resp.json()}    id
    Dictionary Should Contain Key    ${resp.json()}    createdAt
    Dictionary Should Contain Item    ${resp.json()}    user    robert
    Dictionary Should Contain Item    ${resp.json()}    job    cook

Successful Login
    ${request}    Create Dictionary    email=eve.holt@reqres.in    password=cityslicka
    ${resp}    POST On Session    reqres    /api/login    json=${request}    expected_status=200
    Dictionary Should Contain Key    ${resp.json()}    token

Unsuccessful Login
    ${request}    Create Dictionary    email=peter@klaven
    POST On Session    reqres    /api/login    json=${request}    expected_status=400

Successful Registration
    ${request}    Create Dictionary    email=eve.holt@reqres.in    password=pistol
    ${resp}    POST On Session    reqres    /api/register    json=${request}    expected_status=200
    Dictionary Should Contain Key    ${resp.json()}    id
    Dictionary Should Contain Key    ${resp.json()}    token

Unsuccessful Registration
    ${request}    Create Dictionary    email=sydney@fife
    ${resp}    POST On Session    reqres    /api/register    json=${request}    expected_status=400
    Dictionary Should Contain Item    ${resp.json()}    error    Missing password
