*** Settings ***
Resource        ../../../Resources/Settings.resource

Suite Setup     Create Session    reqres    ${ENDPOINT}    verify=True

*** Variables ***
${ENDPOINT}     https://reqres.in

*** Test Cases ***
List Users
    [Documentation]    Tests a simple get request to reqres server.
    ${resp}    GET On Session    reqres    /api/users    params=page=2    expected_status=200
    Dictionary Should Contain Item    ${resp.json()}    page    2
    Dictionary Should Contain Item    ${resp.json()}    per_page    6

Single User Not Found
    [Documentation]    Tests Status Code of User that has not been found.
    ${resp}    GET On Session    reqres    /api/users/23    expected_status=404
    Should Be Equal    Not Found    ${resp.reason}
